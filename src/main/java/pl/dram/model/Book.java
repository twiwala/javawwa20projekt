package pl.dram.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Book implements Serializable {
    private int id;
    private String title;
    private String author;
    private boolean isLend;

    public static final Comparator<Book> COMPARATOR_TITLE = (x,y) -> x.getTitle().compareTo(y.getTitle());
}
