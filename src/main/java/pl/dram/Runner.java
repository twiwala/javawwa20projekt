package pl.dram;

import pl.dram.gui.impl.ConsoleGui;
import pl.dram.gui.Gui;

public class Runner {
    public static void main(String[] args) {
        Gui gui = new ConsoleGui();
        gui.showMainMenu();
    }
}
