package pl.dram.dao;

import pl.dram.model.Book;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public interface BookDao {
    List<Book> getAll();
    List<Book> getAllSortBy(Comparator<Book> comparator);
    List<Book> getAllBy(Predicate<Book> bookPredicate);
    void add(Book book);
    void save();

    void lendBook(Integer id);
}
