package pl.dram.service;

import pl.dram.dao.BookDao;
import pl.dram.model.Book;

import java.util.List;

public class BookService {
    private BookDao bookDao;

    public BookService(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public List<Book> getAll() {
        return bookDao.getAll();
    }
    public List<Book> getAllNotLendBooks(){
        return bookDao.getAllBy(e -> !e.isLend());
    }
    public List<Book> getAllByTitle(){
        return bookDao.getAllSortBy(Book.COMPARATOR_TITLE);
    }

    public void addBookAndSave(Book book){
        bookDao.add(book);
        bookDao.save();;
    }
    public void lendBook(Integer id){
        bookDao.lendBook(id);
    }
    public void save() {
        bookDao.save();
    }
}
