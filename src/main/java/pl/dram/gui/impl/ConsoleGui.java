package pl.dram.gui.impl;

import pl.dram.dao.BookDao;
import pl.dram.dao.impl.BookDaoImpl;
import pl.dram.gui.Gui;
import pl.dram.service.BookService;

import java.util.Scanner;

public class ConsoleGui implements Gui {
    private static Scanner scanner = new Scanner(System.in);

    private BookService bookService = new BookService(new BookDaoImpl());

    @Override
    public void showMainMenu() {
        Integer chosed = null;
        do {
            printMainOptions();

            chosed = getInt();
            switch (chosed) {
                case 0:
                    System.out.println("Bye bye :)");
                    break;
                case 1:
                    System.out.println(bookService.getAll());
                    break;
                case 2:
                    System.out.println(bookService.getAllByTitle());
                    break;
                case 3:
                    bookService.save();
                    System.out.println("Zapisano :))");
                    break;
                case 4:
                    System.out.println(bookService.getAllNotLendBooks());
                    break;
                case 5:
                    System.out.println("Wybierz ksiazke ktora chcesz wypozyczyc");
                    System.out.println("bookService.getAllNotLendBooks() = " + bookService.getAllNotLendBooks());
                    Integer ksiazkaDoWypozyczenia = getInt();
                    bookService.lendBook(ksiazkaDoWypozyczenia);
                    break;
                default:
                    System.out.println("Sprobuj ponownie - nieznana opcja :(");
            }
        } while (chosed != 0);

    }

    private void printMainOptions() {
        System.out.println("0 - Wyjscie");
        System.out.println("1 - Wyswietl ksiazki");
        System.out.println("2 - Wyswietl ksiazki posortowane po tytule");
        System.out.println("3 - Zapisz ksiazki");
        System.out.println("4 - Wyswietl ksiazki do wypozyczenia");
        System.out.println("5 - Wypozycz ksiazke");
    }

    private Integer getInt() {
        String s = scanner.nextLine();
        return Integer.parseInt(s);
    }
}
