package pl.dram.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.dram.dao.impl.BookDaoImpl;
import pl.dram.model.Book;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookServiceTest {
    private BookService bookService;

    @BeforeEach
    void beforeEach() {
        bookService = new BookService(new BookDaoImpl());
    }

    @Test
    void shouldReturnEmptyBooksWhenNoBooksAdded(){
        List<Book> all = bookService.getAll();

        assertTrue(all.isEmpty());
    }
    @Test
    void shouldAddBooksWhenNoBooksAdded(){
        Book book = new Book(1, "Potop", "Sienkiewicz", true);

        bookService.addBookAndSave(book);

        List<Book> all = bookService.getAll();

        assertTrue(all.contains(book));
    }
}