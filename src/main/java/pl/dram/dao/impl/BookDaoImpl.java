package pl.dram.dao.impl;

import pl.dram.dao.BookDao;
import pl.dram.model.Book;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BookDaoImpl implements BookDao {
    private List<Book> books;

/*
    private static BookDaoImpl instance = new BookDaoImpl();

    public static BookDaoImpl getInstance() {
        return instance;
    }
*/

    public BookDaoImpl() {
        this.books = new ArrayList<>();
        this.books.add(new Book(1, "Quo Vadis", "Sienkiewicz", false));
        this.books.add(new Book(2, "Potop", "Sienkiewicz", false));
    }

    @Override
    public List<Book> getAll() {
        return new ArrayList<>(books);
    }

    @Override
    public List<Book> getAllSortBy(Comparator<Book> comparator) {
        return books.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<Book> getAllBy(Predicate<Book> bookPredicate) {
        return books.stream()
                .filter(bookPredicate)
                .collect(Collectors.toList());
    }

    @Override
    public void add(Book book) {
        this.books.add(book);
    }

    @Override
    public void save() {
        try(OutputStream outputStream = Files.newOutputStream(Paths.get("books.ser"));
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)){
            objectOutputStream.writeObject(books);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void lendBook(Integer id) {
        final Optional<Book> any = books.stream()
                .filter(e -> e.getId() == id)
                .findAny();
        any.get().setLend(true);
    }
}
